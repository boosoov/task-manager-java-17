package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.User;

public interface IAuthRepository {

    String getUserId();

    void logIn(User user);

    void logOut();

}
