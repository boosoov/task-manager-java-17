package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void create(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneById(String userId, String id);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneById(String userId, String id);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

    void load(Collection<Project> projects);

    void load(Project... projects);

    Project merge(Project project);

    void merge(Collection<Project> projects);

    void merge(Project...projects);

    void clearAll();

    List<Project> getListProjects();

}
