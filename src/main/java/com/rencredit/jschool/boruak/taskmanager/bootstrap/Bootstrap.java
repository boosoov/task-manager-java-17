package com.rencredit.jschool.boruak.taskmanager.bootstrap;

import com.rencredit.jschool.boruak.taskmanager.api.repository.*;
import com.rencredit.jschool.boruak.taskmanager.api.service.*;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownCommandException;
import com.rencredit.jschool.boruak.taskmanager.repository.*;
import com.rencredit.jschool.boruak.taskmanager.service.*;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalCommandUtil;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;

import java.io.IOException;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository(this);

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthRepository authRepository = new AuthRepository();

    private final IAuthService authService = new AuthService(userService, authRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IDomainService domainService = new DomainService(this);

    public void run(final String[] args) {
        String[] commands = null;
        initUsers();
        commandService.registryCommand();
        System.out.println("** WELCOME TO TASK MANAGER ** \n");
        parseArgs(args);
        while (true) {
            try {
                commands = TerminalUtil.nextLine().split("\\s+");
                parseArgs(commands);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void initUsers() {
        userService.add("test", "test");
        userService.add("admin", "admin", Role.ADMIN);
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        for (String arg : args) {
            processArg(TerminalCommandUtil.convertArgumentToCommand(arg));
            System.out.println();
        }
    }

    private void processArg(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commandService.getTerminalCommands().get(cmd);
        if (command == null) throw  new UnknownCommandException(cmd);
        authService.checkRoles(command.roles());
        try {
            command.execute();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

}
