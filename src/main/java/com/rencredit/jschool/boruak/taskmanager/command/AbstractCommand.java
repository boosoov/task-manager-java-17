package com.rencredit.jschool.boruak.taskmanager.command;

import com.rencredit.jschool.boruak.taskmanager.api.repository.*;
import com.rencredit.jschool.boruak.taskmanager.api.service.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.repository.*;
import com.rencredit.jschool.boruak.taskmanager.service.*;

import java.io.IOException;

public abstract class AbstractCommand {

    protected ICommandService commandService;

    protected IUserService userService;

    protected IAuthService authService;

    protected ITaskService taskService;

    protected IProjectService projectService;

    protected IDomainService domainService;

    public AbstractCommand() {
    }

    public void setCommandService(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    public void setAuthService(IAuthService authService) {
        this.authService = authService;
    }

    public void setTaskService(ITaskService taskService) {
        this.taskService = taskService;
    }

    public void setProjectService(IProjectService projectService) {
        this.projectService = projectService;
    }

    public void setDomainService(IDomainService domainService) {
        this.domainService = domainService;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String arg();

    public abstract String name();

    public abstract  String description();

    public abstract void execute() throws IOException, ClassNotFoundException;

}
