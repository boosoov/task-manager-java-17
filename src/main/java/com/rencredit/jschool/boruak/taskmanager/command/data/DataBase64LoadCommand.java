package com.rencredit.jschool.boruak.taskmanager.command.data;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.constant.DataConstant;
import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataBase64LoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-base64-load";
    }

    @Override
    public String description() {
        return "Save data from base64 file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BASE64 LOAD]");
        final String base64Data = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_BASE64)));
        final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64Data);
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        domainService.load(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
