package com.rencredit.jschool.boruak.taskmanager.command.project;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;

import java.util.List;

public class ProjectListShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show projects list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECTS]");
        final String userId = authService.getUserId();
        final List<Project> projects = projectService.findAll(userId);
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN, Role.USER};
    }

}
