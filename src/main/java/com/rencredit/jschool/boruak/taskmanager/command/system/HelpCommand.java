package com.rencredit.jschool.boruak.taskmanager.command.system;

import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;

import java.util.Map;

public class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "Display terminal command.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Map<String, AbstractCommand> commands = commandService.getTerminalCommands();
        for(Map.Entry<String, AbstractCommand> command : commands.entrySet()){
            final StringBuilder resultString = new StringBuilder();
            if (command.getValue().name() != null && !command.getValue().name().isEmpty()) resultString.append(command.getValue().name());
            if (command.getValue().arg() != null && !command.getValue().arg().isEmpty()) resultString.append(", ").append(command.getValue().arg());
            if (command.getValue().description() != null && !command.getValue().description().isEmpty()) resultString.append(": ").append(command.getValue().description());
            System.out.println(resultString.toString());
        }
        System.out.println("[OK]");
    }

}
