package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;

public class ProfileShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "view-profile";
    }

    @Override
    public String description() {
        return "Show profile.";
    }

    @Override
    public void execute() {
        final String userId = authService.getUserId();
        final User user = userService.getById(userId);
        System.out.println(user);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN, Role.USER};
    }

}
