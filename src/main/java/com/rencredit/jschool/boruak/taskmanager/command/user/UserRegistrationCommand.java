package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;

public class UserRegistrationCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "registration";
    }

    @Override
    public String description() {
        return "Registration in system.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        final User user = authService.registration(login, password);
        System.out.println(user);
    }

}
