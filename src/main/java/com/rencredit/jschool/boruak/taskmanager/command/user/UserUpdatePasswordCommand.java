package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "update-password";
    }

    @Override
    public String description() {
        return "Update password.";
    }

    @Override
    public void execute() {
        System.out.println("Enter new password");
        final String password = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final User user = userService.updatePasswordById(userId, password);
        System.out.println(user);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN, Role.USER};
    }

}
