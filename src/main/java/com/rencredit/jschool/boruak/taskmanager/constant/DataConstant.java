package com.rencredit.jschool.boruak.taskmanager.constant;

public interface DataConstant {

    public static final String FILE_BINARY = "./data.bin";

    public static final String FILE_BASE64 = "./data.base64";

    public static final String FILE_XML = "./data.xml";

    public static final String FILE_JSON = "./data.json";



}
