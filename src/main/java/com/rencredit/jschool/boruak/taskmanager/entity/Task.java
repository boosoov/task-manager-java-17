package com.rencredit.jschool.boruak.taskmanager.entity;

import java.io.Serializable;
import java.util.UUID;

public class Task implements Serializable {

    public static final long serialVersionUID = 1;

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private String userId;

    public Task() {
    }

    public Task(final String title) {
        this.name = title;
    }

    public Task(final String title, final String description) {
        this.name = title;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }

}
