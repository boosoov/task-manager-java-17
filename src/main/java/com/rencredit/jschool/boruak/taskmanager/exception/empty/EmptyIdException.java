package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}
