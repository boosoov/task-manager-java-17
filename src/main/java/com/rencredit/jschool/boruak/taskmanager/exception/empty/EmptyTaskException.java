package com.rencredit.jschool.boruak.taskmanager.exception.empty;

public class EmptyTaskException extends RuntimeException {

    public EmptyTaskException() {
        super("Error! Task not exist...");
    }

}
