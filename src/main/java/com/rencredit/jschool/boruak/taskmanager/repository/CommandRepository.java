package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.*;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.command.data.*;
import com.rencredit.jschool.boruak.taskmanager.command.user.*;
import com.rencredit.jschool.boruak.taskmanager.command.user.auth.UserLogOutCommand;
import com.rencredit.jschool.boruak.taskmanager.command.user.auth.UserLoginCommand;
import com.rencredit.jschool.boruak.taskmanager.command.project.*;
import com.rencredit.jschool.boruak.taskmanager.command.system.*;
import com.rencredit.jschool.boruak.taskmanager.command.task.*;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommandRepository implements ICommandRepository {

    private final ServiceLocator serviceLocator;

    private static final Class[] COMMANDS = new Class[]{
        HelpCommand.class, SystemInfoCommand.class, ArgumentListShowCommand.class,
        CommandListShowCommand.class, DeveloperInfoShowCommand.class, VersionShowCommand.class,
        SystemExitCommand.class, UserUpdatePasswordCommand.class, ProfileShowCommand.class,
        UserEditProfileCommand.class, UserLogOutCommand.class, UserLoginCommand.class,
        UserRegistrationCommand.class, UserLockCommand.class, UserUnlockCommand.class,
        UserDeleteCommand.class,

        ProjectListClearCommand.class, ProjectCreateCommand.class, ProjectRemoveByIdCommand.class,
        ProjectRemoveByIndexCommand.class, ProjectRemoveByNameCommand.class, ProjectShowByIdCommand.class,
        ProjectShowByIndexCommand.class, ProjectShowByNameCommand.class, ProjectListShowCommand.class,
        ProjectUpdateByIdCommand.class, ProjectUpdateByIndexCommand.class,

        TaskListClearCommand.class, TaskCreateCommand.class, TaskRemoveByIdCommand.class,
        TaskRemoveByIndexCommand.class, TaskRemoveByNameCommand.class, TaskShowByIdCommand.class,
        TaskShowByIndexCommand.class, TaskShowByNameCommand.class, TaskListShowCommand.class,
        TaskUpdateByIdCommand.class, TaskUpdateByIndexCommand.class,

        DataBinaryClearCommand.class, DataBinaryLoadCommand.class, DataBinarySaveCommand.class,
        DataBase64ClearCommand.class, DataBase64LoadCommand.class, DataBase64SaveCommand.class,
    };

    public CommandRepository(final ServiceLocator serviceLocator) {
         this.serviceLocator = serviceLocator;
    }

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void registryCommand() {
        for (final Class clazz : COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                registry(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
    protected ICommandService commandService;

    protected IUserService userService;

    protected ITaskService taskService;

    protected IProjectService projectService;

    protected IDomainService domainService;

    private void registry(final AbstractCommand command) {
        if (command == null) return;
//        command.setServiceLocator(serviceLocator.getProjectService());
        command.setAuthService(serviceLocator.getAuthService());
        command.setCommandService(serviceLocator.getCommandService());
        command.setUserService(serviceLocator.getUserService());
        command.setTaskService(serviceLocator.getTaskService());
        command.setProjectService(serviceLocator.getProjectService());
        command.setDomainService(serviceLocator.getDomainService());
        commands.put(command.name(), command);
    }

    public String[] getCommands() {
        final String[] result = new String[commands.size()];
        int index = 0;
        for (Map.Entry<String, AbstractCommand> command : commands.entrySet()) {
            final StringBuilder resultString = new StringBuilder();
            if (command.getValue().name() != null && !command.getValue().name().isEmpty())
                resultString.append(command.getValue().name());
            if (command.getValue().description() != null && !command.getValue().description().isEmpty())
                resultString.append(": ").append(command.getValue().description());
            result[index] = resultString.toString();
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs() {
        final String[] result = new String[commands.size()];
        int index = 0;
        for (Map.Entry<String, AbstractCommand> command : commands.entrySet()) {
            final StringBuilder resultString = new StringBuilder();
            if (command.getValue().arg() != null && !command.getValue().arg().isEmpty())
                resultString.append(command.getValue().arg());
            if (command.getValue().description() != null && !command.getValue().description().isEmpty())
                resultString.append(": ").append(command.getValue().description());
            result[index] = resultString.toString();
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public Map<String, AbstractCommand> getTerminalCommands() {
        return commands;
    }

}
