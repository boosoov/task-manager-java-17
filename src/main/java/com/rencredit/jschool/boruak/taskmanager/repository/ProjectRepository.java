package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final String userId, final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public Project remove(final String userId, final Project project) {
        projects.remove(project);
        return project;
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> userProject = new ArrayList<>();
        for (Project project : projects) {
            if (userId.equals(project.getUserId())) {
                userProject.add(project);
            }
        }
        return userProject;
    }

    @Override
    public void clear(final String userId) {
        for (int i = 0; i < projects.size(); i++) {
            if (userId.equals(projects.get(i).getUserId())) {
                projects.remove(projects.get(i));
            }
        }
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        for (final Project project : projects) {
            if (userId.equals(project.getUserId()) && id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        final Project project = findOneById(userId, id);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        remove(userId, project);
        return project;
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        final List<Project> userProjects = new ArrayList<>();
        for (Project project : projects) {
            if (userId.equals(project.getUserId())) userProjects.add(project);
        }
        return userProjects.get(index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        for (final Project project : projects) {
            if (userId.equals(project.getUserId()) &&
                    name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        final Project project = findOneByName(userId, name);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        remove(userId, project);
        return project;
    }

    @Override
    public void load(Collection<Project> projects) {
        clearAll();
        merge(projects);
    }

    @Override
    public void load(Project... projects) {
        clearAll();
        merge(projects);
    }

    @Override
    public Project merge(Project project) {
        if (project == null) return null;
        projects.add(project);
        return project;
    }

    @Override
    public void merge(Collection<Project> projects) {
        for (final Project project : projects) merge(project);
    }

    @Override
    public void merge(Project... projects) {
        for (final Project project : projects) merge(project);
    }

    @Override
    public void clearAll() {
        projects.clear();
    }

    @Override
    public List<Project> getListProjects() {
        return projects;
    }

}
