package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ITaskRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public Task remove(final String userId, final Task task) {
        tasks.remove(task);
        return task;
    }

    @Override
    public void load(Collection<Task> tasks) {
        clearAll();
        merge(tasks);
    }

    @Override
    public void load(Task... tasks) {
        clearAll();
        merge(tasks);
    }

    @Override
    public Task merge(final Task task) {
        if (task == null) return null;
        tasks.add(task);
        return task;
    }

    @Override
    public void merge(Collection<Task> tasks) {
        for (final Task task : tasks) merge(task);
    }

    @Override
    public void merge(Task...tasks) {
        for (final Task task : tasks) merge(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> userTasks = new ArrayList<>();
        for (Task task : tasks) {
            if (userId.equals(task.getUserId())) {
                userTasks.add(task);
            }
        }
        return userTasks;
    }

    @Override
    public void clearAll() {
        tasks.clear();
    }

    @Override
    public void clear(final String userId) {
        for (int i = 0; i < tasks.size(); i++) {
            if (userId.equals(tasks.get(i).getUserId())) {
                tasks.remove(tasks.get(i));
            }
        }
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId()) && id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        final Task task = findOneById(userId, id);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        remove(userId, task);
        return task;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        final List<Task> userTasks = new ArrayList<>();
        for (Task task : tasks) {
            if (userId.equals(task.getUserId())) userTasks.add(task);
        }
        return userTasks.get(index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId()) &&
                    name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        remove(userId, task);
        return task;
    }

    @Override
    public List<Task> getListTasks() {
        return tasks;
    }

}
