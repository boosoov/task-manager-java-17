package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IProjectService;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistProjectsListException;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        final Project project = new Project(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        final Project project = new Project(name, description);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null || Objects.isNull(project)) throw new EmptyProjectException();

        projectRepository.add(userId, project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null || Objects.isNull(project)) throw new EmptyProjectException();

        projectRepository.remove(userId, project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        return projectRepository.findAll(userId);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        projectRepository.clear(userId);
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public Project updateProjectById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        final Project project = findOneById(userId, id);
        if (project == null) throw new EmptyProjectException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        final Project project = findOneByIndex(userId, index);
        if (project == null || Objects.isNull(project)) throw new EmptyProjectException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        return projectRepository.findOneById(userId, id);
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        return projectRepository.removeOneById(userId, id);
    }

    @Override
    public void load(Collection<Project> project) {
        if (project == null || Objects.isNull(project)) throw new NotExistProjectsListException();
        projectRepository.load(project);
    }

    @Override
    public void load(Project... projects) {
        if (projects == null) throw new NotExistProjectsListException();
        projectRepository.load(projects);
    }

    @Override
    public Project merge(Project project) {
        if (project == null || Objects.isNull(project)) throw new EmptyProjectException();
        return projectRepository.merge(project);
    }

    @Override
    public void merge(Collection<Project> projects) {
        if (projects == null) throw new NotExistProjectsListException();
        projectRepository.merge(projects);
    }

    @Override
    public void merge(Project... projects) {
        if (projects == null) throw new NotExistProjectsListException();
        projectRepository.merge(projects);
    }

    @Override
    public void clearAll() {
        projectRepository.clearAll();
    }

    @Override
    public List<Project> getListProjects() {
        return projectRepository.getListProjects();
    }

}
