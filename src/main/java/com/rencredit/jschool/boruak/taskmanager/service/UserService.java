package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IUserRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUsersListException;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User getByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User add(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        final String passwordHash = HashUtil.getHashLine(password);
        if (Objects.isNull(passwordHash)) throw new DeniedAccessException();
        final User user = new User(login, passwordHash);
        return add(login, user);
    }

    @Override
    public User add(final String login, final String password, final String firstName) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        final String passwordHash = HashUtil.getHashLine(password);
        if (Objects.isNull(passwordHash)) throw new DeniedAccessException();
        final User user = new User(login, passwordHash, firstName);
        return add(login, user);
    }

    @Override
    public User add(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null || Objects.isNull(role)) throw new EmptyRoleException();


        final String passwordHash = HashUtil.getHashLine(password);
        if (Objects.isNull(passwordHash)) throw new DeniedAccessException();
        final User user = new User(login, passwordHash, role);
        return add(login, user);
    }

    private User add(final String login, final User user) {
        if(userRepository.findByLogin(login) != null) throw new BusyLoginException();
        return userRepository.add(user);
    }

    @Override
    public User updatePasswordById(final String id, final String newPassword) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();

        final String passwordHash = HashUtil.getHashLine(newPassword);
        if (Objects.isNull(passwordHash)) throw new DeniedAccessException();

        final User user = userRepository.findById(id);
        user.setPasswordHash(passwordHash);
        return user;
    }

    @Override
    public User editProfileById(final String id, final String firstName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();

        final User user = userRepository.findById(id);
        user.setFirstName(firstName);
        return user;
    }

    @Override
    public User editProfileById(final String id, final String firstName, final String lastName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();

        final User user = userRepository.findById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User editProfileById(
            final String id,
            final String email,
            final String firstName,
            final String lastName,
            final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();

        final User user = userRepository.findById(id);
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setFirstName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User removeByUser(final User user) {
        if (user == null || Objects.isNull(user)) throw new EmptyUserException();
        return userRepository.removeByUser(user);
    }

    @Override
    public User lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = userRepository.findByLogin(login);
        if(user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = userRepository.findByLogin(login);
        if(user == null) return null;
        user.setLocked(false);
        return user;
    }

    @Override
    public void load(Collection<User> users) {
        if (users == null) throw new NotExistUsersListException();
        userRepository.load(users);
    }

    @Override
    public void load(User... users) {
        if (users == null) throw new NotExistUsersListException();
        userRepository.load(users);
    }

    @Override
    public User merge(User user) {
        if (user == null || Objects.isNull(user)) throw new EmptyUserException();
        return userRepository.merge(user);
    }

    @Override
    public void merge(Collection<User> users) {
        if (users == null) throw new NotExistUsersListException();
        userRepository.merge(users);
    }

    @Override
    public void merge(User... users) {
        if (users == null) throw new NotExistUsersListException();
        userRepository.merge(users);
    }

    @Override
    public void clearAll() {
        userRepository.clearAll();
    }

    @Override
    public List<User> getListUsers() {
        return userRepository.getListUsers();
    }

}
