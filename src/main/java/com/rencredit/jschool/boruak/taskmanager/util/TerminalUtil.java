package com.rencredit.jschool.boruak.taskmanager.util;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;

import java.util.Objects;
import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        String str =  SCANNER.nextLine();
        return str;
    }

    static Integer nextNumber() {
        final String line = nextLine();
        try {
            return Integer.parseInt(line);
        } catch (Exception e) {
            throw new IncorrectIndexException(line);
        }
    }

    static void showProject(final Project project) {
        if (project == null || Objects.isNull(project)) throw new EmptyProjectException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    static void showTask(final Task task) {
        if (task == null || Objects.isNull(task)) throw new EmptyTaskException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

}
